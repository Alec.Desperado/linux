﻿# Linux scripts

* - Server initial setup ->  
    ```bash
    apt update && apt install curl -y && curl -OL https://gitlab.com/Alec.Desperado/linux/raw/main/scripts/server_initial_setup && bash server_initial_setup -S
    ```
* - Server initial User setup ->  
    ```bash
    curl -OL https://gitlab.com/Alec.Desperado/linux/raw/main/scripts/server_initial_user_setup && bash server_initial_user_setup -n
    ```
* - Install Docker ->  
    ```bash
    curl -L https://bit.ly/docker_installer | bash
    ```
        -OR-
    ```bash
    curl -OL https://gitlab.com/Alec.Desperado/linux/raw/main/scripts/install_docker && bash install_docker
    ```


* - Cloud-Config ->  
    https://cloudinit.readthedocs.io/  
    ```bash
    #cloud-config
    package_update: true
    package_upgrade: true
    packages:
      - git
      - python3-pip
    ```

```

Generate bash code. Check and sort out potential bugs and edge cases. Optimise the code for performance. Include comments explaining key points.
Generate bash code. Check and fix potential bugs and edge cases. Optimise code to improve performance. Include comments explaining key points.
Generate Bash code. Fix bugs and edge cases. Optimize for performance. Include comments.


```
